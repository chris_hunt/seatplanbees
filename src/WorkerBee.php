<?php
namespace Seatplan;

class WorkerBee extends AbstractBee
{
    
    public $type = "worker";
    
    public $initialLifePoints = 75;
    
    public $hitPoints = 10;
    
}