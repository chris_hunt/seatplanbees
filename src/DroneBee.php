<?php
namespace Seatplan;

class DroneBee extends AbstractBee
{
    public $type = "drone";
    
    public $initialLifePoints = 50;
    
    public $hitPoints = 12;
    
}