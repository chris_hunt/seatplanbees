<?php
namespace Seatplan;

class QueenBee extends AbstractBee
{
    
    public $type = "queen";
    
    public $initialLifePoints = 100;
    
    public $hitPoints = 8;
    
}