<?php

namespace Seatplan;

abstract class AbstractBee
{
    public $beeId;
    
    public $lifePoints;
    
    public $hitPoints;
    
    public function __construct($lifePoints = null)
    {
        $this->beeId = uniqid();
        
        if ($lifePoints != null) {
            $this->lifePoints = $lifePoints;
        } else {
            $this->lifePoints = $this->initialLifePoints;
        }
    }  
    
    public function hit()
    {
       $this->lifePoints = $this->lifePoints - $this->hitPoints;
    }
    
    public function updateLifePoints($points)
    {
        $this->lifePoints = $points;
    }
    
    public function isDead()
    {
        if ($this->lifePoints <= 0) {
            return true;
        } else {
            return false;
        }
    }
}