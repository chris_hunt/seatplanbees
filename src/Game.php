<?php

namespace Seatplan;

class Game
{
    public $gameOver = false;
    
    public $bees = [];
    
    public $feedback = '';
    
    private $initialState = [
       "queens" => 1,
       "workers" => 5,
       "drones" => 8
    ];
    
    public function __construct() 
    {
    }
     
    public function hitRandomBee()
    {
        $totalBees = count($this->bees);
        $beeHit = rand(0, $totalBees - 1);
        $hitBee = $this->bees[$beeHit];
        $hitBee->hit();
        $this->feedback = "Bee with type " . $hitBee->type . " and ID " . $hitBee->beeId . " hit.";
        if ($hitBee->isDead()) {
            if ($hitBee->type == 'queen') {
                $this->gameOver = true;
                foreach ($this->bees as $bee) {
                    $bee->updateLifePoints(0);
                }
            }
            unset($this->bees[$beeHit]);
            $this->bees = array_values($this->bees);
        }
    }
    
    public function setupNewGame()
    {
        // Set initial state
        $beeCount = 0;
        
        while ($beeCount < $this->initialState['queens']) {
            
            $this->bees[] = new QueenBee();
            $beeCount ++;
        }
        
        $beeCount = 0;
        while ($beeCount < $this->initialState['workers']) {
            $this->bees[] = new WorkerBee();
            $beeCount ++;
        }
        
        $beeCount = 0;
        while ($beeCount < $this->initialState['drones']) {
            $this->bees[] = new DroneBee();
            $beeCount ++;
        }
    }
    
}