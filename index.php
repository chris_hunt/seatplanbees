<?php

require __DIR__ . '/vendor/autoload.php';


session_start();

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $game = new Seatplan\Game();
    $game->setupNewGame();
    //print serialize($game);
    $_SESSION['gamestate'] = serialize($game);
} else {
    if (array_key_exists('gamestate', $_SESSION)) {
        $game = unserialize($_SESSION['gamestate']);
        $game->hitRandomBee();
        $_SESSION['gamestate'] = serialize($game);
    } 
}

?>
<html>
    <body>
    	<table>
    		<tr>
    			<th>ID</th>
    			<th>Bee type</th>
    			<th>Life points</th>
    		</tr>
    		<?php 
    		  foreach ($game->bees as $bee) {
    		     ?>
    		     	<tr>
    		     		<td><?php echo $bee->beeId; ?></td>
    		     		<td><?php echo $bee->type; ?></td>
    		     		<td><?php echo $bee->lifePoints; ?></td>
    		     	</tr>
    		     <?php 
    		  }
    		?>
    	</table>
    	<?php 
    	   echo "<p>" . $game->feedback ."</p>";
    	   if ($game->gameOver) {
    	       echo "THE GAME IS OVER - Refresh page to restart";
    	   } else {
    	       ?>
    	       <form action="/" method="post">
        	        <input type="submit" value="HIT!">
                </form>
    	       <?php 
    	   }
    	?>
    	
    </body>
</html>