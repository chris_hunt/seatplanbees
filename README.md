### Bees

Refresh page with GET to restart the game.

When the queen is dead, the hit button will disappear and will show game is over.

To run, checkout repo into folder, run `composer install` and then `php -S localhost:8001`. App will then be running on `localhost:8001`